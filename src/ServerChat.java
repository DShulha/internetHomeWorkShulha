import java.io.*;
import java.net.*;


class ServerChat extends Thread {

    private Socket socket;
    private BufferedReader read;
    private BufferedWriter writes;


    public ServerChat(Socket socket) throws IOException {
        this.socket = socket;
        read = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        writes = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        Server.story.printStory(writes);
        start();
    }
    @Override
    public void run() {
        String word;
        try {
            word = read.readLine();// первое сообщение отправленное сюда - это никнейм
            try {
                writes.write(word + "\n");
                writes.flush();
            } catch (IOException ignored) {}
            try {
                while (true) {
                    word = read.readLine();
                    if(word.equals("stop")) {
                        this.downService(); // остановка сервера
                        break;
                    }
                    System.out.println("Echoing: " + word);
                    Server.story.addStoryEl(word);
                    for (ServerChat vr : Server.serverList) {
                        vr.send(word); // отправить принятое сообщение с привязанного клиента всем остальным влючая его
                    }
                }
            } catch (NullPointerException ignored) {}


        } catch (IOException e) {
            this.downService();
        }
    }

    private void send(String msg) {
        try {
            writes.write(msg + "\n");
            writes.flush();
        } catch (IOException ignored) {}

    }

    private void downService() {
        try {
            if(!socket.isClosed()) {
                socket.close();
                read.close();
                writes.close();
                for (ServerChat vr : Server.serverList) {
                    if(vr.equals(this)) vr.interrupt();
                    Server.serverList.remove(this);
                }
            }
        } catch (IOException ignored) {}
    }
}
