import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.Socket;

public class Client {

    JTextArea area;
    JTextField field;
    Socket socket;
    BufferedReader reader;
    BufferedWriter writer;
    InputStreamReader streamReader;

    public static void main(String[] args) {
        Client client = new Client();
        client.start();
    }

    public void start() {
        JFrame frame = new JFrame("ЧАТик");
        JPanel jPanel = new JPanel();

        area = new JTextArea(25, 35);
        area.setLineWrap(true);
        area.setWrapStyleWord(true);
        area.setEditable(false);
        JScrollPane qScroll = new JScrollPane(area);
        area.append("Введите сообщение: " + "\n");
        area.isShowing();

        qScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        qScroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.getContentPane().add(BorderLayout.CENTER, jPanel);

        field = new JTextField(30);
        JButton button = new JButton("Отправить");
        button.addActionListener(new ButtonListener());
        jPanel.add(qScroll);
        jPanel.add(field);
        jPanel.add(button);
        setNet();

        Thread consolThread = new Thread((new IncomeReader()));
        consolThread.setDaemon(true);
        consolThread.start();

        frame.setSize(400, 550);
        frame.setVisible(true);
    }

    private void setNet() {
        try {

            socket = new Socket("localHost", 8010);
            System.out.println("Новое подключение!");

            streamReader = new InputStreamReader(socket.getInputStream());
            reader = new BufferedReader(streamReader);
            writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public class ButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                writer.append(field.getText() + "\n");
                writer.flush();
            } catch (Exception exception) {
                exception.printStackTrace();
            }
            field.setText("");
            field.requestFocus();
        }
    }

    public class IncomeReader implements Runnable {

        @Override
        public void run() {
            String message;

            try {
                while ((message = reader.readLine()) != null) {
                    System.out.println("сообщение: " + message);
                    area.append(message + "\n");

                    if (message.equalsIgnoreCase("exit")) {
                        Client.this.stopChat();
                        break;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private void stopChat() {          //закрывание сервера и прерывание нити
        try {
            if (!socket.isClosed()) {
                socket.close();
                reader.close();
                writer.close();
                this.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

