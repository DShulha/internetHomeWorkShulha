package anotherServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.nio.charset.StandardCharsets;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.*;


public class ServerMain {


    public static void main(String[] args) throws IOException {

        ClientRepositury clientRepositury = new ClientRepositury();

        MessegeQueue messegeQueue = new MessegeQueue();

        List<ClientThread> clients = new CopyOnWriteArrayList<>();// Для работы в многопоточной среде

        Thread massageSenderThread = new Thread(new MassageSander(messegeQueue, clients));
        massageSenderThread.setDaemon(true);
        massageSenderThread.start();

        ExecutorService executorService = Executors.newFixedThreadPool(20);

        try (ServerSocket serverSocket = new ServerSocket(8000)) {

            while (true) {
                try {
                    Socket socket = serverSocket.accept();// ожидание нового подключения
                    System.out.println("Hовое подключение! ");

                    ClientThread client = new ClientThread(socket, messegeQueue, clientRepositury);
                    try {

                        executorService.submit(client);
                        clients.add(client);

                    } catch (RejectedExecutionException e) {
                        client.sendMessage("нет мест!!");
                        socket.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static class Messege {

        private String value;

        private ClientThread client;

        public Messege(String value, ClientThread client) {
            this.value = value;
            this.client = client;
        }

        public String getValue() {
            return value;
        }

        public ClientThread getClient() {
            return client;
        }

        @Override
        public String toString() {
            return this.client != null ? this.client.getName() : "Server " + getValue();
        }
    }

    public static class MessegeQueue {

        private final Deque<Messege> queue = new LinkedList<>();

        public synchronized void add(Messege value) {
            this.queue.add(value);
            notifyAll();
        }

        public synchronized Optional<Messege> getOrEmpty() throws InterruptedException {
            while (this.queue.isEmpty()) {
                wait();
            }
            return Optional.ofNullable(this.queue.poll());
        }

    }

    public static class MassageSander implements Runnable {


        private final MessegeQueue messegeQueue;

        private final List<ClientThread> clients;

        public MassageSander(MessegeQueue messegeQueue, List<ClientThread> clients) {
            this.messegeQueue = messegeQueue;
            this.clients = clients;
        }

        @Override
        public void run() {

            try {

                while (true) {

                    Optional<Messege> messege = this.messegeQueue.getOrEmpty();

                    if (messege.isEmpty()) {
                        Thread.sleep(1000);
                        continue;
                    }

                    String messageText = messege.toString();
                    for (ClientThread client : this.clients) {


                        if (!client.isSign()) {
                            continue;
                        }

                        try {
                            client.sendMessage(messageText);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private static class ClientRepositury {
        private final Map<String, ClientThread> clientThreadByClientName = new ConcurrentHashMap<>();

        public void add(String name, ClientThread clientThread) {
            this.clientThreadByClientName.put(name, clientThread);
        }

        public void removed(String name) {
            this.clientThreadByClientName.remove(name);
        }

        public boolean existByName(String name) {
            return this.clientThreadByClientName.containsKey(name);
        }

    }


    public static class ClientThread implements Runnable {

        private String name;

        private final Socket socket;

        private final OutputStreamWriter writer;

        private final BufferedReader reader;

        private final MessegeQueue messegeQueue;

        private final ClientRepositury clientRepositury;

        public ClientThread(Socket socket, MessegeQueue messegeQueue, ClientRepositury clientRepositury) throws IOException {
            this.socket = socket;
            this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), StandardCharsets.UTF_8));
            this.writer = new OutputStreamWriter(socket.getOutputStream(), StandardCharsets.UTF_8);
            this.messegeQueue = messegeQueue;
            this.clientRepositury = clientRepositury;
        }

        public String getName() {
            return this.name;
        }

        public boolean isSign() {
            return this.name != null
                    && !this.name.isEmpty()
                    && this.clientRepositury.existByName(this.name);
        }

        @Override
        public void run() {

            System.out.println("Новое подключение запущено в новом потоке! " + Thread.currentThread());

            try (this.socket) {

                signIn();

                while (true) {

                    String valueFromClient = reader.readLine();

                    if (valueFromClient.equalsIgnoreCase("exit")) {
                        break;
                    }

                    this.messegeQueue.add(new Messege(valueFromClient, this));
                    writer.append(valueFromClient + "\n");
                    writer.flush();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void sendMessage(String messageText) throws IOException {
            this.writer.append(messageText).append('\n').flush();
        }

        private void signIn() throws IOException {

            sendMessage("Server: Введите логин: ");
            while (true) {
                String messege = this.reader.readLine();
                if (messege.equalsIgnoreCase("exit")) {
                    throw new RuntimeException("Клиент не авторизован!");
                }
                if (messege == null || messege.isEmpty()) {
                    sendMessage("Server: Введите корpектный логин!");
                }

                this.name = messege;
                this.clientRepositury.add(this.name, this);

                sendMessage("Авторизация успешна!");
                this.messegeQueue.add(new Messege(this.name + " " + "теперь в чате ", null));
                break;
            }
        }
    }
}

