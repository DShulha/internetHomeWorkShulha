import java.io.*;
import java.net.*;
import java.util.LinkedList;


public class Server {

    public static final int PORT = 8010;
    public static LinkedList<ServerChat> serverList = new LinkedList<>(); // список всех нитей
    public static StoryChat story; // история переписки


    public static void main(String[] args) throws IOException {
        ServerSocket server = new ServerSocket(PORT);
        story = new StoryChat();
        System.out.println("Сервер запущен!!");
        try {
            while (true) {
                Socket socket = server.accept();
                try {
                    serverList.add(new ServerChat(socket)); // добавление нового соединения в список
                    System.out.println("Принято новое подключение!!");
                } catch (IOException e) {
                    socket.close();
                }
            }
        } finally {
            server.close();
        }
    }
}